package id.co.bca.training.di;

import javax.inject.Singleton;

import dagger.Component;
import id.co.bca.training.view.DetailRecipeActivity;
import id.co.bca.training.view.FavoriteFragment;
import id.co.bca.training.viewmodel.RecipeViewModel;
import id.co.bca.training.viewmodel.repository.RecipeRepository;

@Singleton
@Component(modules = {RecipeModule.class})
public interface RecipeComponent {
    void inject(RecipeRepository recipeRepository);
    void inject(RecipeViewModel recipeViewModel);
    void inject(DetailRecipeActivity detailRecipeActivity);
    void inject(FavoriteFragment favoriteFragment);
}
