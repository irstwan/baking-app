package id.co.bca.training.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.co.bca.data.repository.RecipeRepositoryImpl;
import id.co.bca.data.room.RoomManager;
import id.co.bca.domain.repository.RecipeRepository;
import id.co.bca.domain.usecase.recipe.GetRecipeUseCase;
import id.co.bca.training.entity.mapper.RecipeModelMapper;
import id.co.bca.training.util.RecipeContext;

@Module
public class RecipeModule {

    @Provides
    static RecipeModelMapper recipeModelMapper(){
        return new RecipeModelMapper();
    }

    @Provides
    static RecipeRepository recipeRepository(){
        return new RecipeRepositoryImpl();
    }

    @Provides
    static GetRecipeUseCase getRecipeUseCase(RecipeRepository recipeRepository){
        return new GetRecipeUseCase(recipeRepository);
    }

    @Provides
    @Singleton
    RecipeContext provideMovieContext(){
        return new RecipeContext();
    }

    @Provides
    @Singleton
    RoomManager provideRoomManager(){
        return new RoomManager(RecipeContext.getAppContext());
    }
}
