package id.co.bca.training.entity;


import lombok.Data;

@Data
public class IngredientsModel {
    private String quantity;
    private String measure;
    private String ingredient;
}
