package id.co.bca.training.entity;

import java.util.List;

import lombok.Data;

@Data
public class RecipeRespModel {
    private String image;
    private String servings;
    private String name;
    private List<IngredientsModel> ingredients;
    private int id;
    private List<StepsModel> steps;

}
