package id.co.bca.training.entity;

import lombok.Data;

@Data
public class StepsModel {
    private String videoURL;
    private String description;
    private int id;
    private String shortDescription;
    private String thumbnailURL;
}
