package id.co.bca.training.entity.mapper;


import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import id.co.bca.data.room.entity.RecipeRoom;
import id.co.bca.domain.model.Ingredients;
import id.co.bca.domain.model.RecipeResp;
import id.co.bca.domain.model.Steps;
import id.co.bca.training.entity.IngredientsModel;
import id.co.bca.training.entity.RecipeRespModel;
import id.co.bca.training.entity.StepsModel;

public class RecipeModelMapper {

    private Gson mgson = new Gson();

    public List<RecipeRespModel> recipeEntityToPresentation(List<RecipeResp> param){
        List<RecipeRespModel> recipeRespsList = new ArrayList<>();
        for (int i = 0; i < param.size(); i++){
            RecipeRespModel recipeRespModel = new RecipeRespModel();
            RecipeResp recipeResp = param.get(i);

            recipeRespModel.setServings(recipeResp.getServings());
            recipeRespModel.setName(recipeResp.getName());
            recipeRespModel.setImage(recipeResp.getImage());
            recipeRespModel.setId(recipeResp.getId());

            List<StepsModel> stepsList = new ArrayList<>();
            for (int j = 0; j < recipeResp.getSteps().size(); j++){
                Steps stepsEntity = recipeResp.getSteps().get(j);
                StepsModel steps = new StepsModel();
                steps.setVideoURL(stepsEntity.getVideoURL());
                steps.setThumbnailURL(stepsEntity.getThumbnailURL());
                steps.setShortDescription(stepsEntity.getShortDescription());
                steps.setId(stepsEntity.getId());
                steps.setDescription(stepsEntity.getDescription());
                stepsList.add(steps);
            }
            recipeRespModel.setSteps(stepsList);


            List<IngredientsModel> ingredientsList = new ArrayList<>();
            for (int j = 0; j< recipeResp.getIngredients().size(); j++){
                Ingredients ingredients = recipeResp.getIngredients().get(j);
                IngredientsModel ingredientsModel = new IngredientsModel();
                ingredientsModel.setIngredient(ingredients.getIngredient());
                ingredientsModel.setMeasure(ingredients.getMeasure());
                ingredientsModel.setQuantity(ingredients.getQuantity());
                ingredientsList.add(ingredientsModel);
            }
            recipeRespModel.setIngredients(ingredientsList);

            recipeRespsList.add(recipeRespModel);
        }

        return recipeRespsList;
    }

    public List<RecipeRespModel> recipeRoomToPresentation(List<RecipeRoom> param){
        List<RecipeRespModel> list = new ArrayList<>();
        for (int i =0; i < param.size(); i++){
            RecipeRespModel recipeRespModel = new RecipeRespModel();
            recipeRespModel.setId(param.get(i).getId());
            recipeRespModel.setName(param.get(i).getTitle());
            list.add(recipeRespModel);
        }
        return list;
    }
}
