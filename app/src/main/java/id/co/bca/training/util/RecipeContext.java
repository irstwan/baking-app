package id.co.bca.training.util;

import android.app.Application;
import android.content.Context;

public class RecipeContext extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        RecipeContext.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return RecipeContext.context;
    }
}
