package id.co.bca.training.util;

import java.util.ArrayList;
import java.util.List;

import id.co.bca.training.entity.RecipeRespModel;

public class RecipeSingleton {
    private static RecipeSingleton mInstance;
    private List<RecipeRespModel> list = null;

    public static RecipeSingleton getInstance() {
        if(mInstance == null)
            mInstance = new RecipeSingleton();

        return mInstance;
    }

    private RecipeSingleton() {
        list = new ArrayList<RecipeRespModel>();
    }
    // retrieve array from anywhere
    public List<RecipeRespModel> getList() {
        return this.list;
    }
    //Add element to array
    public void addToList(List<RecipeRespModel> value) {
        list = value;
    }
}
