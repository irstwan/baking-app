package id.co.bca.training.view;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.bca.data.room.entity.RecipeRoom;
import id.co.bca.training.R;
import id.co.bca.training.entity.RecipeRespModel;
import id.co.bca.training.util.RecipeSingleton;
import id.co.bca.training.util.VerticalSpacingItemDecorator;
import id.co.bca.training.view.adapter.IngredientAdapter;
import id.co.bca.training.view.adapter.StepRecyclerAdapter;
import id.co.bca.training.view.adapter.StepRecyclerView;
import id.co.bca.training.viewmodel.RecipeViewModel;

public class DetailRecipeActivity extends AppCompatActivity {
    public static final String KET_RECIPE = "recipe";
    private Gson mGson = new Gson();
    private RecipeViewModel recipeViewModel;
    private RecipeRoom recipeRoom;
    private RecipeRespModel respModel;
    private Menu mGlobalMenu;

    @BindView(R.id.toolbar_detail) Toolbar toolbar;
    @BindView(R.id.rv_ingredients) RecyclerView rvIngredients;
    @BindView(R.id.rv_steps) StepRecyclerView rvSteps;
    @BindView(R.id.tv_title) TextView tvTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_recipe);
        ButterKnife.bind(this);
        setUpToolBar();
        Intent intent = getIntent();
        String jsonResp = intent.getStringExtra(KET_RECIPE);
        respModel = mGson.fromJson(jsonResp, RecipeRespModel.class);
        tvTitle.setText(respModel.getName());

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvSteps.setLayoutManager(layoutManager);
        VerticalSpacingItemDecorator itemDecorator = new VerticalSpacingItemDecorator(10);
        rvSteps.addItemDecoration(itemDecorator);

        rvSteps.setStepList(respModel.getSteps());
        StepRecyclerAdapter adapter = new StepRecyclerAdapter(respModel.getSteps(), initGlide());
        rvSteps.setAdapter(adapter);

        IngredientAdapter mIngredientAdapter = new IngredientAdapter(respModel.getIngredients(), this);
        RecyclerView.LayoutManager layoutIngredient = new LinearLayoutManager(this);
        rvIngredients.setLayoutManager(layoutIngredient);
        rvIngredients.setAdapter(mIngredientAdapter);

        recipeViewModel = ViewModelProviders.of(this).get(RecipeViewModel.class);
        getFavoriteRecipeById(respModel.getId());
    }

    private RequestManager initGlide(){
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.white_background)
                .error(R.drawable.white_background);

        return Glide.with(this)
                .setDefaultRequestOptions(options);
    }

    @Override
    protected void onDestroy() {
        if(rvSteps!=null)
            rvSteps.releasePlayer();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(rvSteps!=null)
            rvSteps.releasePlayer();
        finish();
    }

    protected Toolbar setUpToolBar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        } else {
        }
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(rvSteps!=null)
                        rvSteps.releasePlayer();

                    finish();
                }
            });
        }
        return toolbar;
    }

    private void getFavoriteRecipeById(int id){
        recipeViewModel.getFavoriteRecipeById(id).observe(this, new Observer<RecipeRoom>() {
            @Override
            public void onChanged(RecipeRoom resp) {
                recipeRoom = resp;
                updateOptionMenu();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_favorite, menu);
        mGlobalMenu = menu;
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_favorite);
        if (recipeRoom == null){
            menuItem.setIcon(R.drawable.ic_favorite_border);
        } else {
            menuItem.setIcon(R.drawable.ic_favorite);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        getFavoriteRecipeById(respModel.getId());

        if (recipeRoom == null){
            item.setIcon(R.drawable.ic_favorite_border);
            recipeViewModel.saveFavoriteRecipe(respModel.getId(), respModel.getName());
        } else {
            item.setIcon(R.drawable.ic_favorite);
            recipeViewModel.deleteFavoriteRecipe(respModel.getId());
        }

        return true;
    }

    public void updateOptionMenu(){
        if (mGlobalMenu != null){
            onPrepareOptionsMenu(mGlobalMenu);
        }
    }
}
