package id.co.bca.training.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.bca.data.room.entity.RecipeRoom;
import id.co.bca.training.R;
import id.co.bca.training.di.DaggerRecipeComponent;
import id.co.bca.training.entity.RecipeRespModel;
import id.co.bca.training.entity.mapper.RecipeModelMapper;
import id.co.bca.training.util.RecipeSingleton;
import id.co.bca.training.view.adapter.ClickListener;
import id.co.bca.training.view.adapter.RecipeAdapter;
import id.co.bca.training.viewmodel.RecipeViewModel;

public class FavoriteFragment extends Fragment {
    private RecipeViewModel recipeViewModel;
    private Gson mGson = new Gson();
    private List<RecipeRespModel> respModelList = new ArrayList<>();

    @Inject
    public RecipeModelMapper recipeModelMapper;

    @BindView(R.id.rv_favorite) RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);
        ButterKnife.bind(this, view);
        DaggerRecipeComponent.create().inject(this);
        recipeViewModel = ViewModelProviders.of(this).get(RecipeViewModel.class);
        loadFavorite();
        return view;
    }

    private void loadFavorite(){
        recipeViewModel.getFavoriteMoviesList().observe(this, new Observer<List<RecipeRoom>>() {
            @Override
            public void onChanged(List<RecipeRoom> recipeRooms) {
                respModelList = recipeModelMapper.recipeRoomToPresentation(recipeRooms);
                setUpRecyclerView(respModelList);
            }
        });
    }

    private void setUpRecyclerView(List<RecipeRespModel> respModelList){
        RecipeAdapter recipeAdapter = new RecipeAdapter(respModelList, getActivity());
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager2);
        mRecyclerView.setAdapter(recipeAdapter);

        recipeAdapter.setClickListener(new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                int id = respModelList.get(position).getId() - 1;
                if(!RecipeSingleton.getInstance().getList().isEmpty()){
                    RecipeRespModel recipeRespModel = RecipeSingleton.getInstance().getList().get(id);
                    respModelList.get(position).getId();
                    Intent intent = new Intent(getActivity(), DetailRecipeActivity.class);
                    intent.putExtra(DetailRecipeActivity.KET_RECIPE, mGson.toJson(recipeRespModel));
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "Gagal ambil data", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });
    }
}
