package id.co.bca.training.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.bca.data.room.entity.RecipeRoom;
import id.co.bca.training.R;
import id.co.bca.training.entity.RecipeRespModel;
import id.co.bca.training.view.adapter.ClickListener;
import id.co.bca.training.view.adapter.RecipeAdapter;
import id.co.bca.training.viewmodel.RecipeViewModel;

public class RecipeFragment extends Fragment {
    @BindView(R.id.rv_recipe) RecyclerView mRecyclerView;

    private RecipeViewModel recipeViewModel;
    private List<RecipeRespModel> respModelList = new ArrayList<>();
    private Gson mGson = new Gson();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe, container, false);
        ButterKnife.bind(this, view);

        recipeViewModel = ViewModelProviders.of(this).get(RecipeViewModel.class);
        getRecipe();
        getFav();
        return view;
    }

    private void getRecipe(){
        recipeViewModel.getRecipe().observe(this, new Observer<List<RecipeRespModel>>() {
            @Override
            public void onChanged(List<RecipeRespModel> recipeRespModelList) {
                respModelList = recipeRespModelList;
                setUpRecyclerView();
            }
        });
    }

    private void getFav(){
        recipeViewModel.getFavoriteMoviesList().observe(this, new Observer<List<RecipeRoom>>() {
            @Override
            public void onChanged(List<RecipeRoom> recipeRooms) {
                Log.d("fav -> ", mGson.toJson(recipeRooms));
            }
        });
    }

    private void setUpRecyclerView(){
        RecipeAdapter recipeAdapter = new RecipeAdapter(respModelList, getActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(recipeAdapter);

        recipeAdapter.setClickListener(new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                RecipeRespModel recipeRespModel = respModelList.get(position);
                Intent intent = new Intent(getActivity(), DetailRecipeActivity.class);
                intent.putExtra(DetailRecipeActivity.KET_RECIPE, mGson.toJson(recipeRespModel));
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });
    }
}
