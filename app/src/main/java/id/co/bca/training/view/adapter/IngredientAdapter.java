package id.co.bca.training.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.bca.training.R;
import id.co.bca.training.entity.IngredientsModel;
import id.co.bca.training.entity.StepsModel;

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.ViewHolder> {
    private List<IngredientsModel> mIngredientsList = new ArrayList<>();
    private final Context context;
    private Gson mGson = new Gson();

    public IngredientAdapter(List<IngredientsModel> mIngredientsList, Context context) {
        this.mIngredientsList = mIngredientsList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ingredient, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final IngredientsModel item = mIngredientsList.get(position);
        holder.mIngredientsTv.setText(item.getQuantity()+" "+item.getMeasure()+" "+item.getIngredient());
    }


    @Override
    public int getItemCount() {
        return mIngredientsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_ingredient) TextView mIngredientsTv;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
