package id.co.bca.training.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.bca.training.R;
import id.co.bca.training.entity.RecipeRespModel;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.ViewHolder> {
    private List<RecipeRespModel> mRecipeList = new ArrayList<>();
    private final Context context;
    private ClickListener clickListener;
    private Gson mGson = new Gson();

    public RecipeAdapter(List<RecipeRespModel> mRecipeList, Context context) {
        this.mRecipeList = mRecipeList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recipe, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final RecipeRespModel item = mRecipeList.get(position);
        holder.mRecipeText.setText(item.getName());

        holder.mRecipeText.setOnClickListener(v -> clickListener.onClick(v, position));
    }


    @Override
    public int getItemCount() {
        return mRecipeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_item_recipe) TextView mRecipeText;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setClickListener(ClickListener _clickListener) {
        this.clickListener = _clickListener;
    }
}
