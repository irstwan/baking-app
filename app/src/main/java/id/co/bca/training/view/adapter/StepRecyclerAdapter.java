package id.co.bca.training.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;

import java.util.List;

import id.co.bca.training.R;
import id.co.bca.training.entity.StepsModel;

public class StepRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<StepsModel> stepList;
    private RequestManager requestManager;


    public StepRecyclerAdapter(List<StepsModel> stepList, RequestManager requestManager) {
        this.stepList = stepList;
        this.requestManager = requestManager;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new StepViewHolder(
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_step, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((StepViewHolder)viewHolder).onBind(stepList.get(i), requestManager);
    }
    
    @Override
    public int getItemCount() {
        return stepList.size();
    }

}














