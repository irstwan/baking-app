package id.co.bca.training.view.adapter;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.bca.training.R;
import id.co.bca.training.entity.StepsModel;


public class StepViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.media_container) FrameLayout media_container;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.thumbnail) ImageView thumbnail;
    @BindView(R.id.volume_control) ImageView volumeControl;

    @BindView(R.id.progressBar) ProgressBar progressBar;
    View parent;
    RequestManager requestManager;

    public StepViewHolder(@NonNull View itemView) {
        super(itemView);
        parent = itemView;
        ButterKnife.bind(this, itemView);
    }

    public void onBind(StepsModel stepsModel, RequestManager requestManager) {
        this.requestManager = requestManager;
        parent.setTag(this);
        title.setText(stepsModel.getDescription());

        if (stepsModel.getVideoURL().equals("")){
            media_container.setVisibility(View.GONE);
        } else {
            thumbnail.setVisibility(View.VISIBLE);
            this.requestManager
                    .load(R.drawable.ic_thumbnail)
                    .into(thumbnail);
        }
    }

}














