package id.co.bca.training.viewmodel;

import java.util.List;

import id.co.bca.training.entity.RecipeRespModel;

public interface RecipeInterface {
    interface RequiredRecipeOps {
        void onResultRecipeSuccess(List<RecipeRespModel> recipeRespModelList);
        void onResultRecipeError(String message);
    }

    interface ProvidedRecipeOps {
        void getRecipe();

        void onDestroy();
    }
}
