package id.co.bca.training.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import javax.inject.Inject;

import id.co.bca.data.room.RoomManager;
import id.co.bca.data.room.entity.RecipeRoom;
import id.co.bca.training.di.DaggerRecipeComponent;
import id.co.bca.training.entity.RecipeRespModel;
import id.co.bca.training.viewmodel.repository.RecipeRepository;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class RecipeViewModel extends AndroidViewModel implements RecipeInterface.RequiredRecipeOps {
    private RecipeInterface.ProvidedRecipeOps providedRecipeOps;
    private MutableLiveData<List<RecipeRespModel>> recipeRespModelMutableLiveData;

    @Inject
    public RoomManager roomManager;

    public RecipeViewModel(@NonNull Application application) {
        super(application);
        DaggerRecipeComponent.create().inject(this);
        providedRecipeOps = new RecipeRepository(this);
    }

    public LiveData<List<RecipeRespModel>> getRecipe(){
        if (recipeRespModelMutableLiveData == null){
            recipeRespModelMutableLiveData = new MutableLiveData<>();
            providedRecipeOps.getRecipe();
        }
        return recipeRespModelMutableLiveData;
    }

    @Override
    public void onResultRecipeSuccess(List<RecipeRespModel> recipeRespModel) {
        recipeRespModelMutableLiveData.setValue(recipeRespModel);
    }

    @Override
    public void onResultRecipeError(String message) {

    }

    public void saveFavoriteRecipe(int id, String name){
        roomManager.insertFavotite(new RecipeRoom(id, name))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }
                });
    }

    public void deleteFavoriteRecipe(int id){
        roomManager.deleteFavoriteRecipe(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    public LiveData<RecipeRoom> getFavoriteRecipeById(int id){
        return roomManager.getFavoritesRecipeById(id);
    }

    public LiveData<List<RecipeRoom>> getFavoriteMoviesList(){
        return roomManager.getFavoritesRecipe();
    }

}
