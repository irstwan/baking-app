package id.co.bca.training.viewmodel.repository;

import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import id.co.bca.domain.usecase.recipe.GetRecipeUseCase;
import id.co.bca.training.di.DaggerRecipeComponent;
import id.co.bca.training.entity.RecipeRespModel;
import id.co.bca.training.entity.mapper.RecipeModelMapper;
import id.co.bca.training.util.RecipeSingleton;
import id.co.bca.training.viewmodel.RecipeInterface;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class RecipeRepository implements RecipeInterface.ProvidedRecipeOps {
    private RecipeInterface.RequiredRecipeOps requiredRecipeOps;
    private CompositeDisposable mDisposable;
    private Gson gson = new Gson();
    @Inject
    public GetRecipeUseCase getRecipeUseCase;
    @Inject
    public RecipeModelMapper recipeModelMapper;

    public RecipeRepository(RecipeInterface.RequiredRecipeOps requiredRecipeOps) {
        this.requiredRecipeOps = requiredRecipeOps;
        DaggerRecipeComponent.create().inject(this);
        mDisposable = new CompositeDisposable();
    }

    @Override
    public void getRecipe() {
        mDisposable.add(
                getRecipeUseCase.execute()
                        .map(recipeModelMapper::recipeEntityToPresentation)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<List<RecipeRespModel>>() {
                            @Override
                            public void onSuccess(List<RecipeRespModel> recipeRespModelList) {
                                RecipeSingleton.getInstance().addToList(recipeRespModelList);
                                requiredRecipeOps.onResultRecipeSuccess(recipeRespModelList);
                            }

                            @Override
                            public void onError(Throwable e) {

                            }
                        })
        );
    }

    @Override
    public void onDestroy() {

    }
}
