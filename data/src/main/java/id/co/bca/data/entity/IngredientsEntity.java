package id.co.bca.data.entity;

import lombok.Data;

@Data
public class IngredientsEntity {
    private String quantity;
    private String measure;
    private String ingredient;
}
