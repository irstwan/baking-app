package id.co.bca.data.entity;

import java.util.List;

import lombok.Data;

@Data
public class RecipeRespEntity {
    private String image;
    private String servings;
    private String name;
    private List<IngredientsEntity> ingredients;
    private int id;
    private List<StepsEntity> steps;

}
