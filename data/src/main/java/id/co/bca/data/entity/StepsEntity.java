package id.co.bca.data.entity;

import lombok.Data;

@Data
public class StepsEntity {
    private String videoURL;
    private String description;
    private int id;
    private String shortDescription;
    private String thumbnailURL;
}
