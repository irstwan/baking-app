package id.co.bca.data.entity.mapper;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import id.co.bca.data.entity.IngredientsEntity;
import id.co.bca.data.entity.RecipeRespEntity;
import id.co.bca.data.entity.StepsEntity;
import id.co.bca.domain.model.Ingredients;
import id.co.bca.domain.model.RecipeResp;
import id.co.bca.domain.model.Steps;

public class RecipeMapper {
    private Gson mGson = new Gson();

    public List<RecipeResp> recipeEntityToDomain(List<RecipeRespEntity> param){
        List<RecipeResp> recipeRespsList = new ArrayList<>();
        for (int i = 0; i < param.size(); i++){
            RecipeResp recipeResp1 = new RecipeResp();
            RecipeRespEntity recipeResp = param.get(i);

            recipeResp1.setServings(recipeResp.getServings());
            recipeResp1.setName(recipeResp.getName());
            recipeResp1.setImage(recipeResp.getImage());
            recipeResp1.setId(recipeResp.getId());

            List<Steps> stepsList = new ArrayList<>();
            for (int j = 0; j < recipeResp.getSteps().size(); j++){
                StepsEntity stepsEntity = recipeResp.getSteps().get(j);
                Steps steps = new Steps();
                steps.setVideoURL(stepsEntity.getVideoURL());
                steps.setThumbnailURL(stepsEntity.getThumbnailURL());
                steps.setShortDescription(stepsEntity.getShortDescription());
                steps.setId(stepsEntity.getId());
                steps.setDescription(stepsEntity.getDescription());
                stepsList.add(steps);
            }
            recipeResp1.setSteps(stepsList);


            List<Ingredients> ingredientsList = new ArrayList<>();
            for (int j = 0; j < recipeResp.getIngredients().size(); j++){
                IngredientsEntity ingredientsEntity = recipeResp.getIngredients().get(j);
                Ingredients ingredients = new Ingredients();
                ingredients.setIngredient(ingredientsEntity.getIngredient());
                ingredients.setMeasure(ingredientsEntity.getMeasure());
                ingredients.setQuantity(ingredientsEntity.getQuantity());
                ingredientsList.add(ingredients);
            }
            recipeResp1.setIngredients(ingredientsList);

            recipeRespsList.add(recipeResp1);
        }

        return recipeRespsList;
    }
}
