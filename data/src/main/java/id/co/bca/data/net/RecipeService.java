package id.co.bca.data.net;
import java.util.List;

import id.co.bca.data.entity.RecipeRespEntity;
import io.reactivex.Single;
import retrofit2.http.GET;

public interface RecipeService {
    @GET("topher/2017/May/59121517_baking/baking.json")
    Single<List<RecipeRespEntity>> getRecipeResp();
}
