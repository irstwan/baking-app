package id.co.bca.data.net;

public class ServiceGenerator {
    private static RecipeService recipeService;
    private static String BASE_URL = "http://d17h27t6h515a5.cloudfront.net/";
    public static RecipeService getRecipeService(){
        if (recipeService ==null){
            recipeService = RetrofitHelper.getRetrofit(BASE_URL).create(RecipeService.class);
        }
        return recipeService;
    }
}
