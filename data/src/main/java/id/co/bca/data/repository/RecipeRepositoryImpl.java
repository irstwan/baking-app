package id.co.bca.data.repository;
import java.util.List;

import id.co.bca.data.entity.mapper.RecipeMapper;
import id.co.bca.data.net.RecipeService;
import id.co.bca.data.net.ServiceGenerator;
import id.co.bca.domain.model.RecipeResp;
import id.co.bca.domain.repository.RecipeRepository;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class RecipeRepositoryImpl implements RecipeRepository {
    private final Scheduler backgroundScheduler;
    private RecipeMapper recipeMapper;

    public RecipeRepositoryImpl() {
        this.backgroundScheduler = Schedulers.io();
        this.recipeMapper = new RecipeMapper();
    }

    @Override
    public Single<List<RecipeResp>> doGetRecipe() {
        RecipeService recipeService = ServiceGenerator.getRecipeService();
        return Single.defer(recipeService::getRecipeResp)
                .map(recipeMapper::recipeEntityToDomain)
                .subscribeOn(backgroundScheduler);
    }
}
