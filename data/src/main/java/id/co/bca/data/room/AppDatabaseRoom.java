package id.co.bca.data.room;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

;import id.co.bca.data.room.entity.RecipeDao;
import id.co.bca.data.room.entity.RecipeRoom;

@Database(entities = {RecipeRoom.class}, version = 1, exportSchema = false)
public abstract class AppDatabaseRoom extends RoomDatabase {
    private static final String LOG_TAG = AppDatabaseRoom.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "favorite";
    private static AppDatabaseRoom sInstance;

    public static AppDatabaseRoom getInstance(Context context) {
        if (sInstance == null){
            synchronized (LOCK) {
                if (sInstance == null){

                    final Migration MIGRATION_1_2 = new Migration(1, 2) {
                        @Override
                        public void migrate(@NonNull SupportSQLiteDatabase database) {
                        }
                    };

                    Log.d(LOG_TAG, "Creating new database instance");
                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabaseRoom.class, AppDatabaseRoom.DATABASE_NAME)
//                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        Log.d(LOG_TAG, "Getting the database instance");
        return sInstance;
    }
    public abstract RecipeDao recipeDao();
}

