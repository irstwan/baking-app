package id.co.bca.data.room;

import android.content.Context;

import androidx.lifecycle.LiveData;

import java.util.List;

import id.co.bca.data.room.entity.RecipeDao;
import id.co.bca.data.room.entity.RecipeRoom;
import io.reactivex.Completable;

public class RoomManager {

    private RecipeDao recipeDao;

    public RoomManager(Context context) {
        AppDatabaseRoom appDatabaseRoom = AppDatabaseRoom.getInstance(context);
        recipeDao = appDatabaseRoom.recipeDao();
    }

    public Completable insertFavotite(RecipeRoom recipeRoom){
        return recipeDao.insertRecipe(recipeRoom);
    }

    public LiveData<List<RecipeRoom>> getFavoritesRecipe(){
        return recipeDao.getAllRecipe();
    }

    public LiveData<RecipeRoom> getFavoritesRecipeById(int id){
        return recipeDao.getRecipeById(id);
    }

    public Completable deleteFavoriteRecipe(int id){
        return recipeDao.deleteRecipeById(id);
    }
}
