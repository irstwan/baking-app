package id.co.bca.data.room.entity;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Completable;

@Dao
public interface RecipeDao {
    @Insert
    Completable insertRecipe(RecipeRoom recipeRoom);

    @Query("SELECT * FROM tbl_recipe")
    LiveData<List<RecipeRoom>> getAllRecipe();

    @Query("SELECT * FROM TBL_RECIPE WHERE id=:id")
    LiveData<RecipeRoom> getRecipeById(int id);

    @Query("DELETE FROM tbl_recipe WHERE id = :id")
    Completable deleteRecipeById(int id);
}
