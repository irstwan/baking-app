package id.co.bca.data.room.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Entity(tableName = "tbl_recipe")
public class RecipeRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
}
