package id.co.bca.data;

import org.junit.Test;

import java.util.List;

import id.co.bca.data.entity.RecipeRespEntity;
import id.co.bca.data.net.RecipeService;
import id.co.bca.data.net.ServiceGenerator;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

public class RecipeTest {
    @Test
    public void T001_GetPopularMovies(){
        RecipeService recipeService = ServiceGenerator.getRecipeService();
        Single<List<RecipeRespEntity>> resp = recipeService.getRecipeResp();

        TestObserver<List<RecipeRespEntity>> testObserver = new TestObserver<>();
        resp.subscribe(testObserver);
        testObserver.assertComplete();
        testObserver.assertNoErrors();
    }
}
