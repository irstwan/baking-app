package id.co.bca.domain.model;


import lombok.Data;

@Data
public class Ingredients {
    private String quantity;
    private String measure;
    private String ingredient;
}
