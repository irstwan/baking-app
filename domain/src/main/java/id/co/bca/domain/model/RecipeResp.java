package id.co.bca.domain.model;

import java.util.List;

import lombok.Data;

@Data
public class RecipeResp {
    private String image;
    private String servings;
    private String name;
    private List<Ingredients> ingredients;
    private int id;
    private List<Steps> steps;

}
