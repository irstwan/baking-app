package id.co.bca.domain.model;

import lombok.Data;

@Data
public class Steps {
    private String videoURL;
    private String description;
    private int id;
    private String shortDescription;
    private String thumbnailURL;
}
