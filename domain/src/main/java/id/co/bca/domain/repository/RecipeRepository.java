package id.co.bca.domain.repository;

import java.util.List;

import id.co.bca.domain.model.RecipeResp;
import io.reactivex.Single;

public interface RecipeRepository {
    Single<List<RecipeResp>> doGetRecipe();
}
