package id.co.bca.domain.usecase;

import io.reactivex.Single;

public interface SingleUseCase<T> {
    Single<T> execute();
}
