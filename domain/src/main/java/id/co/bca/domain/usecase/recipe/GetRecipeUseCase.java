package id.co.bca.domain.usecase.recipe;

import java.util.List;

import id.co.bca.domain.model.RecipeResp;
import id.co.bca.domain.repository.RecipeRepository;
import id.co.bca.domain.usecase.SingleUseCase;
import io.reactivex.Single;

public class GetRecipeUseCase implements SingleUseCase<List<RecipeResp>> {
    private final RecipeRepository recipeRepository;

    public GetRecipeUseCase(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @Override
    public Single<List<RecipeResp>> execute() {
        return Single.defer(recipeRepository::doGetRecipe);
    }
}
